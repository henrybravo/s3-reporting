import datetime
import boto3
import os
from email import encoders
from email.mime.base import MIMEBase
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

def get_s3_object_tags(bucket, key):
    # retrieve the Tags for each object found in the specified AWS S3 buckets
    try:
        client = boto3.client('s3')
        response = client.get_object_tagging(
            Bucket=bucket,
            Key=key,
        )
        # get Key Values from dict in list
        s = [','.join(map(str,i.values())) for i in response['TagSet']]
        # convert to csv string
        r = (','.join(str(a)for a in s))
        # convert to items list
        list = r.split(",") if r else []
        # extract Value items from list
        tags = list[1::2]
        # create csv string to append to .list_objects call
        return (','.join(tags))
    except Exception as ex:
        print(ex)

def get_s3_object_etag(bucket, key):
    # retreive the etag with the md5sum object filechecksum
    try:
        client = boto3.client('s3')
        response = client.head_object(
            Bucket=bucket,
            Key=key
        )
        return('{}'.format(response['ETag'].strip('"')))
    except Exception as ex:
        print(ex)

def lambda_handler(event, context):
    # set the region for us
    aws_region = 'eu-west-1'
    ##################################################
    ### ACTION: define the S3 Buckets to report on ###
    ##################################################
    datalake_s3_buckets = [
        'my-bucket',
    ]
    ##########################################################
    ### ACTION: define the S3 Buckets to upload csv report ###
    ##########################################################
    reportss3bucket = 'data-analytics-reports-bucket'
    ########################################
    ### ACTION: set to 0 after first run ###
    ########################################
    verify_email = 0
    verification_email_address = 'reporting-list@email.com'
    ########################################
    ### ACTION: set the email adress(es) ###
    ########################################
    msg_subject = 'AWS Datalake S3 usage report'
    msg_from = 'datalake-reporting-aws-s3@mail.com'
    msg_to = verification_email_address
    # load the Amazon Simple Email Service (SES) boto3 module
    ses_client = boto3.client('ses', region_name = aws_region)
    # load the Amazon S3 boto3 module
    client = boto3.client('s3')
    # one time email verification requirement
    if verify_email:
        response = ses_client.verify_email_identity(
            EmailAddress = verification_email_address
        )
        print(response)
    # generate the report
    print('==> Generating report')
    today = datetime.datetime.now()
    date_time = today.strftime("%m-%d-%Y_%H-%M")
    output_filepath = '/tmp/'
    output_filename = 'aws_s3_report'
    output_extention = '.csv'
    reportcsv = output_filepath + aws_region + '_' + output_filename + '_' + date_time + output_extention
    reportfilename = aws_region + '_' + output_filename + '_' + date_time + output_extention
    # write the csv data file to /tmp/ local file storage
    try:
        fileio = open(reportcsv, 'w')
        with fileio as outfile:
            # write the csv header
            outfile.write(
                'Column1,Column2,Column3,Column4,Column5,Column6,Column7,Column8,Column9,Column10'
            )
            # collect the objects in each S3 bucket and get the attributes
            for bucket in datalake_s3_buckets:
                response = client.list_objects(
                    Bucket = bucket,
                )
                # collect attributes for each objects
                for i in response['Contents']:
                    objectkey = i['Key']
                    objectsize = i['Size']
                    etag = get_s3_object_etag(bucket,objectkey)
                    storageclass = i['StorageClass']
                    # our tagging strategy requires 5x tags applied to each object in our datalake
                    ourtags = get_s3_object_tags(bucket,objectkey)
                    if ourtags == "":
                        ourtags = 'null'
                    # write attributes for each object to csv data file
                    outfile.write('\n{},{},{},{},{},{}'.format(
                        bucket,
                        objectkey,
                        objectsize,
                        etag,
                        storageclass,
                        ourtags)
                    )
        fileio.close()
    except Exception as ex:
        print(ex)
    # send report to email
    try:
        print('==> Sending report ' '{}' ' to email subscribers {}'.format(reportfilename,msg_to))
        msg = MIMEMultipart()
        msg["Subject"] = msg_subject
        msg["From"] = msg_from
        msg["To"] = msg_to
        html = """\
            <html>
              <head></head>
              <body>
                <h1>AWS S3 datalake usage report</h1>
                <p>
                   Filename: """ +str(reportfilename)+ """
                </p>
              </body>
            </html>
        """
        msg.attach(MIMEText(html, 'html'))
        with open(reportcsv, "rb") as attachment:
            part = MIMEApplication(attachment.read())
            part.add_header("Content-Disposition",
                            "attachment",
                            filename = reportcsv)
        msg.attach(part)
        # message to string and send
        response = ses_client.send_raw_email(
            Source = msg_from,
            Destinations = [ msg_to ],
            RawMessage = {"Data": msg.as_string()}
        )
        print(response)
    except Exception as ex:
        print(ex)
    # put report in S3 Bucket for QuickSight reporting
    try:
        print('==> Uploading S3 object with SSE-KMS')
        s3 = boto3.resource('s3')
        reportss3objectkey = 'data-analytics-bi-and-data-portal-s3-report.csv'
        s3.meta.client.upload_file(
            reportcsv,
            reportss3bucket,
            reportss3objectkey,
            ExtraArgs={'ServerSideEncryption':'aws:kms'}
        )
        print('==> Report ' '{}' ' uploaded to S3 bucket ' '{}'.format(reportfilename,reportss3bucket))
    except Exception as ex:
        print(ex)