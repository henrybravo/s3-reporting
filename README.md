# aws-s3-reporting

This solution deploys a serverless event-driven workload that enables reporting on your datalake based on AWS S3. You can enable a schedule, for example on a daily basis, that will execute a Lambda function that will scan through your S3 datalake and will generate a report to provide details on the current data (S3 objects) size, the file checksum, along with the associated object tags. In this example we consider a tagging strategy to be fixed to 5x tags applied to each file (S3 object). The report is sent as a CSV attached file to the preconfigure reciepients e-mail address. 

Secondly a copy of the report is uploaded to a 'reporting' S3 bucket that will allow as a data source for a QuickSight reporting dashboard. 

AWS services Lambda and EventBridge form the main building blocks and are deployed using the AWS Serverless Application Model.

![](AWS_S3_datalake_report.png)

## Prerequisites

- Install and configure AWS CLI
    - Installing or updating the latest version of the AWS CLI: [getting-started-install](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
    - Configuring the AWS CLI: [aws configure](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html#cli-configure-quickstart-config)
- Install SAM CLI: [sam](https://aws.amazon.com/serverless/sam/)
- Ensure python 3.8 is installed as the Lambda will run on **ARM64 architecture powered by Amazon Graviton processors**
- 2x AWS S3 Buckets for 1/ SAM artifacts and 2/ CSV file upload

## Deploying the solution

1. After cloning this repository, you need to change the email address that you would like to use to send the reports to.

2. Then run:

`sam build`

`sam validate`

`sam deploy --guided`

3. Then we need to switch off the email validation requirement for AWS SNS by modifying the `s3-reporting/app.py` file in line 63 and set the value to 0.

4. Then we build and deploy the package again propagating the change

`sam build`

`sam deploy --guided`

4. The solution is now ready and it will execute at 6pm UTC and send out a report to the email address

## Support
You can drop me a line at info@henrybravo.nl

## Roadmap
Add Glue and Athena resources to the CloudFormation stack for further automation

## Contributing
You are welcome to open a merge request if you think you can bring something interesting (feature) or an improvement or possible bug fix.

## Authors and acknowledgment
Solution developed by Henry Bravo.

## License
GNU General Public License version 3 [GPL-3.0 ](https://opensource.org/licenses/GPL-3.0)
